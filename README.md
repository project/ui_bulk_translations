User Interface Bulk Translations (ui_bulk_translations)
---------------------

This module has been created to facilitate the task of translating the strings
stored in the User interface. It allows you to use a translation provider to send
requests in batches and automatically translate the entire string library into any available language.

REQUIREMENTS
------------

This module requires TMGMT (http://drupal.org/project/tmgmt) module to be
installed  and to have created a translation provider, such as Deepl Pro or Deepl Free.

The module requires also the Language and Locale modules enabled, both should be installed as a dependency of TMGMT.
It is also necessary to add at least one language in '/admin/config/regional/language' apart from the default language.

CONFIGURATION
-------------

- add a new translation provider at /admin/tmgmt/translators
- set additional settings related to the provider.
