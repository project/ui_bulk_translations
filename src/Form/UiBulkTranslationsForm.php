<?php

namespace Drupal\ui_bulk_translations\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ui_bulk_translations\Utility\UiBulkTranslationsHelper;
use Exception;

/**
 * Implements an example form.
 */
class UiBulkTranslationsForm extends FormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'ui_bulk_translations.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ui_bulk_translations';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $action_in_progress = $config->get('action_in_progress');

    $providers = \Drupal::entityTypeManager()->getStorage('tmgmt_translator')->loadMultiple();
    $provider_options = [];
    foreach ($providers as $provider) {
      $provider_options[$provider->id()] = $provider->label();
    }

    $form['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Select provider'),
      '#options' => $provider_options
    ];

    $languages = \Drupal::languageManager()->getLanguages();
    $language_options = [];
    foreach ($languages as $language) {
      if ($language->getId() != 'en') {
        $language_options[$language->getId()] = $language->getName();
      }
    }

    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Select language'),
      '#options' => $language_options,
      '#description' => t("This process may take a few minutes. If there is a translation operation in progress, the submit button will be disabled.")
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate Translatable Strings'),
      '#disabled' => $action_in_progress == true ? 'disabled' : ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   * deepl request https://www.deepl.com/docs-api/translate-text
   * set user interface translations https://drupal.stackexchange.com/questions/214803/is-there-a-way-to-programmatically-add-translations-for-strings
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /**
     * TODO try to do the request in batch.inc file and during the process set a variable to lock submit the form
     * until the process ends.
     */
    $config = $this->configFactory->getEditable(static::SETTINGS);

    $action_in_progress = $config->get('action_in_progress');

    if ($action_in_progress) {
      $form_state->setValue('submit', NULL);
      $form_state->setRebuild();
    }
    else {
      $storage = \Drupal::service('locale.storage');
      $ui_strings = UiBulkTranslationsHelper::getUserInterfaceTranslatableStrings($form_state->getValue('language'));
      $target_lang = strtoupper($form_state->getValue('language'));

      $providers = \Drupal::entityTypeManager()->getStorage('tmgmt_translator')->loadMultiple();
      $selected_provider = $form_state->getValue('provider');

      // check all the providers to get the config of the selected provider
      foreach ($providers as $provider) {

        if ($provider->id() == $selected_provider) {
          $provider_settings = $provider->getSettings();
          $url = $provider_settings["url"];
          $auth_key = $provider_settings["auth_key"];
        }
      }

      if(!empty($auth_key) && !empty($url)) {

        $string_batch = [];
        $counter = 0;
        $total_characters = 0;
        foreach ($ui_strings as $key => $string) {

          $string_batch[$key] = $string;
          $counter++;
          $characters_in_current_string = strlen($string);
          $total_characters += $characters_in_current_string;

          /**
           * if the total characters are greater than 9999
           * or the number of the elements are the same than the total processed and less than the limit of characters
           * then try the request.
           */
          if ($total_characters >= 9999 || (count($ui_strings) == $counter && $total_characters < 9999)) {
            $config->set('action_in_progress', true)->save();
            $translated_batch = $this->requestTranslation($string_batch, $target_lang, $auth_key, $url);

            // if the request is not empty o failed
            if (!empty($translated_batch)) {

              foreach ($translated_batch as $lid => $translated_string) {
                try
                {
                  $storage->createTranslation([
                    'lid' => $lid,
                    'language' => $target_lang,
                    'translation' => $translated_string,
                  ])->save();
                }
                catch (Exception $e) {
                  \Drupal::logger('ui_bulk_translations')->error($e);
                }
              }
            }
            else {
              \Drupal::logger('ui_bulk_translations')->warning('Bad request trying to request UI translations.');
            }

            $string_batch = [];
            $counter = 0;
            $total_characters = 0;
          }
        }
      }

      $config->set('action_in_progress', false)->save();
      $selected_language = \Drupal::service('language_manager')->getLanguageName(strtolower($target_lang));
      \Drupal::messenger()->addMessage("All the Strings has been translated to: " . $selected_language . ".");
    }

  }

  /**
   * @param $string_batch
   * @param $target_lang
   * @param $auth_key
   * @param $url
   *
   * @return mixed
   *
   * @see https://www.deepl.com/docs-api/translate-text
   */
  private function requestTranslation($string_batch, $target_lang, $auth_key, $url) {
    $data = [
      'text' => [],
      'lids' => [],
      'target_lang' => $target_lang
    ];

    $string_lids = [];
    foreach ($string_batch as $lid => $string) {
      $data['text'][] = $string;
      $string_lids[] = $lid;
    }

    $json_data = json_encode($data);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'Authorization: DeepL-Auth-Key ' . $auth_key
    ]);

    $json_response = curl_exec($ch);
    $response = json_decode($json_response);

    $string_array = $response->translations->text;

    return array_combine($string_lids, $string_array);
  }
}
