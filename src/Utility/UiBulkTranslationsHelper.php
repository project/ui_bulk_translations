<?php

namespace Drupal\ui_bulk_translations\Utility;


/**
 * Class SmeUiTranslationsHelper
 *
 * @package Drupal\ui_bulk_translations\Helper
 */
class UiBulkTranslationsHelper {

  /**
   * @param $target_lang
   *
   * @return array
   */
  public static function getUserInterfaceTranslatableStrings($target_lang): array {
    $locale_storage = \Drupal::service('locale.storage');
    $user_interface_elements = $locale_storage->getStrings();

    $translatable_strings = [];
    foreach ($user_interface_elements as $string) {
      $has_translation = self::checkIfHasTranslation($string->source, $target_lang);
      if(!$has_translation) {
        $translatable_strings[$string->lid] = $string->source;
      }
    }

    return $translatable_strings;
  }

  private static function checkIfHasTranslation($string, $target_lang): bool {
    $translation = \Drupal::translation()->translate($string, [], ['langcode' => $target_lang]);

    return $translation->render() == $string ? false : true;
  }

}



